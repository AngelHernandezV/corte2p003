function Limpiar() {

    var lblEnganche = document.getElementById('lblEnganche');
    lblEnganche.innerHTML = "Enganche: $";

    var lblTotal = document.getElementById('lblTotal');
    lblTotal.innerHTML = "Total a Financiar: $";

    var lblPagoMensual = document.getElementById('lblPagoMensual');
    lblPagoMensual.innerHTML = "Pago Mensual: $";

}


function Automovil() {
    Limpiar();

    var valor = document.getElementById('valor').value;
    var enganche = (valor * 0.30)
    var total = 0;
    var meses = document.getElementById('meses').value;
    var intereses = 0;
    var pagoMensual = 0;

    var lblenganche = document.getElementById('lblEnganche');
    console.log("Enganche: " + enganche);
    lblenganche.innerHTML = lblenganche.innerHTML + enganche;

    if (meses == 12) {
        intereses = valor * 0.125;
        console.log("Intereses: " + intereses);
    } else if (meses == 18) {
        intereses = valor * 0.172;
        console.log("Intereses: " + intereses);
    } else if (meses == 24) {
        intereses = valor * 0.25;
        console.log("Intereses: " + intereses);
    } else if (meses == 36) {
        intereses = valor * 0.36;
        console.log("Intereses: " + intereses);
    } else if (meses == 42) {
        intereses = valor * 0.45;
        console.log("Intereses: " + intereses);
    }


    var lblTotal = document.getElementById('lblTotal');
    total = valor - enganche + intereses;
    lblTotal.innerHTML = lblTotal.innerHTML + total;


    var lblPagoMensual = document.getElementById('lblPagoMensual');
    pagoMensual = total / meses;
    lblPagoMensual.innerHTML = lblPagoMensual.innerHTML + pagoMensual;

}
